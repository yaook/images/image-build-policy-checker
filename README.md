
# Checker for Yaook image policy.

## Build

```
cd src
docker build --tag image-policy-checker .
```

## Test

```
docker run --volume "$(pwd)/test":/srv/data -w /srv/data image-policy-checker --check-expectations
```

## Apply

```
docker run --volume PATH_TO_DIRECTORY_WITH_DOCKERFILES:/srv/data -w /srv/data image-policy-checker [--no-hints]
```

Dockerfiles are identified by file names starting with `Dockerfile`.
