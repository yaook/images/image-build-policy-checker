#!/usr/bin/env python

"""
Approximate checker for Yaook image policy.

To learn how to apply it, consult the README.

To learn how to adjust existing policies or add new policies, read
the following hints:

* Policies are distinguished into strict policies, which can be checked
  rather accurately, and hinting policies, which are considerably risky
  for false positives.  Hence hinting policies can be suppressed e. g.
  in a CI pipeline using a command switch.

* To register a function as (implementation of) a policy, use the
  decorators `strict_policy` and `hinting_policy`.

* A policy implementation is fed with an abstract representation
  of a dockerfile, in first approximation a sequence of stages
  with a stage represented as a series of commands; see class
  `Dockerfile` and class `Command` for details.

* If a policy implementation detects a violation, it yields the
  command where the violation occurs, plus an optional specific
  description.  See class `Policy` for details.

* For RUN instructions, an approximate tokenization of its argument
  is given in attribute `Command.tokens`. Using this may result in
  more robust implementations of policies on shell commands, study
  existing policy implementations to get an idea how this can be
  accomplished.

For details, study the section headed »Policy framework« for foundations
and the existing policy implementations in the section headed »Concrete
policies«.
"""


import typing
from typing import TypeVar, Callable, Tuple, Optional, Any, Iterator
from dataclasses import dataclass
import enum
from enum import Enum
import itertools
from functools import partial
import re
import textwrap
from pathlib import Path
import shlex
import io
import os
import sys
import argparse
import argcomplete  # type: ignore

from dockerfile_parse import DockerfileParser


# Basics

T = TypeVar('T')


def message(txt: str) -> None:
    """
    Writes `txt` to standard error channel.
    """
    print(txt, file=sys.stderr)


def fail(txt: str, exit_code: int = 1) -> None:
    """
    Writes `txt` via `message` and exits process with exit code
    `exit_code`.
    """
    message(f'ERROR: {txt}')
    raise SystemExit(exit_code)


def split_before(predicate: Callable[[T], bool], elems: list[T]) \
        -> list[list[T]]:
    """
    Splits `elems` such that each element satisfying `predicate`
    heads a partition. All partitions are non-empty. Empty `elems`
    result in `[]`.
    """

    if not elems:
        return []
    indices = [index for index, elem in enumerate(elems)
               if predicate(elem) or index == 0] + [len(elems)]
    return [elems[i:j] for i, j in itertools.pairwise(indices)]


def split_after(predicate: Callable[[T], bool], elems: list[T]) \
        -> list[list[T]]:
    """
    Splits `elems` such that each element satisfying `predicate`
    concludes a partition. All partitions are non-empty. Empty `elems`
    result in `[]`.
    """
    return [list(reversed(partition)) for partition in
            reversed(split_before(predicate, list(reversed(elems))))]


def last(seq: Iterator[T]) -> Optional[T]:
    """
    Computes last element of a finite `seq`. Returns `None` iff `seq`
    does not yield anything.
    """
    elems = list(seq)
    return None if not elems else elems[-1]


def is_prefix(xs: list[T], ys: list[T]) -> bool:
    """Computes whether `xs` is a prefix of `ys`."""
    return xs == ys[:len(xs)]


def is_infix(xs: list[T], ys: list[T]) -> bool:
    """Computes whether `xs` is an infix of `ys`."""
    return any(ys[i:i + len(xs)] == xs for i in range(len(ys) - len(xs) + 1))


# Policy framework

class Instruction(Enum):
    """
    Abstract representation of instructions in a dockerfile,
    including comments.
    """
    COMMENT = enum.auto()
    ADD = enum.auto()
    ARG = enum.auto()
    CMD = enum.auto()
    COPY = enum.auto()
    ENTRYPOINT = enum.auto()
    ENV = enum.auto()
    EXPOSE = enum.auto()
    FROM = enum.auto()
    HEALTHCHECK = enum.auto()
    LABEL = enum.auto()
    MAINTAINER = enum.auto()
    ONBUILD = enum.auto()
    RUN = enum.auto()
    SHELL = enum.auto()
    STOPSIGNAL = enum.auto()
    USER = enum.auto()
    VOLUME = enum.auto()
    WORKDIR = enum.auto()


@dataclass
class Command:
    """
    Abstract representation of a command in a dockerfile.

    :param instruction:         The instruction proper.
    :param value:               The »arguments« to the instruction.
    :param tokens:              For RUN instructions, an approximate
                                tokenization of the given shell command.
    :param comment:             Comment lines preceeding the instruction.
    :param line_start:          Number line where command starts.
    :param line_end:            Number line where command ends.
    :param disabled_policies:   Policies disabled by directives in
                                comment lines.
    """
    instruction: Instruction
    value: str
    tokens: list[str]
    comment: list[str]
    line_start: int
    line_end: int
    disabled_policies: set[str]


@dataclass
class Dockerfile:
    """
    Abstract representation of dockerfile.

    :param p:       Dockerfile path.
    :param stages:  Commands grouped by stages.  The first element might
                    be degenerate consisting of `Instruction.ARG`s only.
                    The last stage might conclude with an auxiliary
                    `Instruction.COMMENT` containing trailing comments.
                    Always non-empty.
    """
    p: Path
    stages: list[list[Command]]


Policy_Evaluation = Iterator[Tuple[Command, list[str]]]
Policy_Implementation = Callable[[Dockerfile], Policy_Evaluation]


@dataclass
class Policy:
    """
    Abstract representation of a policy.

    :param name:    Name of the policy.
    :param doc:     Line-wise description of the policy.
    :param apply:   Implementation of the policy: given a `Dockerfile`,
                    enumerate findings as pairs of commands and a
                    line-wise message; no message lines indicates
                    that the generic description is enough.
    :param strict:  `True` iff policy is strict rather than hinting.
    """
    name: str
    doc: list[str]
    apply: Policy_Implementation
    strict: bool


policies: dict[str, Policy] = {}

re_policy_identifier = re.compile(r'YIP\d\d\d\d')


def memorize_policy(apply: Policy_Implementation, strict: bool) -> Policy:
    global policies
    name = apply.__name__
    assert name not in policies, f'Duplicate policy identifier: {name}'
    assert re_policy_identifier.fullmatch(
        name), f'Bad policy identifier: {name}'
    doc = [s.replace('\n', ' ') for s in
           textwrap.dedent(apply.__doc__ or '').rstrip().split('\n\n')]
    policy = Policy(name=name, doc=doc, apply=apply, strict=strict)
    policies[policy.name] = policy
    return policy


def strict_policy(apply: Policy_Implementation) -> Policy:
    """
    Decorator to register a `Policy_Implementation` as a strict `Policy`.
    The name is introspected from the function name.
    The description is distilled from the dedented docstring such that
    parts separated by one empty source line result in different lines
    in the description afterwards.
    """
    return memorize_policy(apply, strict=True)


def hinting_policy(apply: Policy_Implementation) -> Policy:
    """
    Decorator to register a `Policy_Implementation` as a hinting `Policy`.
    The name is introspected from the function name.
    The description is distilled from the dedented docstring such that
    parts separated by one empty source line result in different lines
    in the description afterwards.
    """
    return memorize_policy(apply, strict=False)


# Concrete policies

debiansuites = ['bullseye', 'bookworm']
re_valid_base_images = re.compile(
    r'^python:3.\d+(.\d+)?-slim-(?:'
    + '|'.join(re.escape(suite) for suite in debiansuites) + ')(?:@.*)?$')


@strict_policy
def YIP0001(dockerfile: Dockerfile) -> Policy_Evaluation:
    """All images SHOULD use python:3.x-slim-$debiansuite as base image."""
    last_from = dockerfile.stages[-1][0]
    if re_valid_base_images.match(last_from.value) is None:
        yield (last_from, YIP0001.doc +
               [f'Known debian suites are {", ".join(debiansuites)}.'])


re_sha256_hash = re.compile(r'@sha256:\w+$')


@strict_policy
def YIP0003(dockerfile: Dockerfile) -> Policy_Evaluation:
    """A CMD instruction SHOULD be provided."""
    last_stage = dockerfile.stages[-1]
    if last(command for command in last_stage
            if command.instruction is Instruction.CMD) is None:
        yield last_stage[0], []


@strict_policy
def YIP0004(dockerfile: Dockerfile) -> Policy_Evaluation:
    """At least the »openstack_release« argument MUST be provided."""
    last_stage = dockerfile.stages[-1]
    if not any(
            command.instruction is Instruction.ARG
            and command.value.startswith('openstack_release=')
            for command in last_stage):
        yield last_stage[0], []


@strict_policy
def YIP0005(dockerfile: Dockerfile) -> Policy_Evaluation:
    """At least the »branch« argument MUST be provided."""
    last_stage = dockerfile.stages[-1]
    if not any(
            command.instruction is Instruction.ARG
            and command.value.startswith('branch=')
            for command in last_stage):
        yield last_stage[0], []


@strict_policy
def YIP0006(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    The »branch« argument MUST not be hardcoded but derived from the
    »openstack_release« argument.
    """
    last_stage = dockerfile.stages[-1]
    some_command = last(
        command for command in last_stage
        if command.instruction is Instruction.ARG
        and command.value.startswith('branch='))
    if some_command is not None:
        branch = some_command.value.removeprefix('branch=')
        if '${openstack_release}' not in branch:
            yield some_command, []


@strict_policy
def YIP0007(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    At most one RUN statement SHOULD be used in (the final stage of) a
    Dockerfile.
    """
    last_stage = dockerfile.stages[-1]
    commands = [
        command for command in last_stage
        if command.instruction is Instruction.RUN
    ]
    if len(commands) > 1:
        yield commands[-1], []


@strict_policy
def YIP0008(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    Within RUN instructions, set -eux and semicolon SHOULD be used to
    concatenate commands.
    """
    for stage in dockerfile.stages:
        for command in stage:
            if ';' in command.tokens and not \
                    is_prefix(['set', '-eux'], command.tokens):
                yield command, []


@strict_policy
def YIP0009(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    Within RUN instructions, semicolon SHOULD be used to concatenate
    commands (not &&).
    """
    for stage in dockerfile.stages:
        for command in stage:
            # Tolerate && after [ … ] and [[ … ]].
            spans = split_before(lambda s: s == '&&', command.tokens)
            if any(span and span[-1] not in [']', ']]']
                    for span in spans[:-1]):
                yield command, []


@strict_policy
def YIP0010(dockerfile: Dockerfile) -> Policy_Evaluation:
    """Python VirtualEnv SHOULD NOT be used in the image."""
    for stage in dockerfile.stages:
        for command in stage:
            if 'venv' in command.tokens:
                yield command, []


apt_upgrade_variants = [
    ['apt', 'upgrade'], ['apt-get', 'upgrade'],
    ['apt', 'dist-upgrade'], ['apt-get', 'dist-upgrade']
]


@strict_policy
def YIP0011(dockerfile: Dockerfile) -> Policy_Evaluation:
    """You MUST NOT update the packages in the image using apt upgrade."""
    for stage in dockerfile.stages:
        for command in stage:
            if any(is_infix(cmd, command.tokens)
                    for cmd in apt_upgrade_variants):
                yield command, []


@strict_policy
def YIP0012(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    If you need to install packages, you should introduce a variable
    named BUILD_PACKAGES containing build-related packages at the start
    of the Dockerfile.
    """
    for stage in dockerfile.stages:
        run_commands = [command for command in stage
                        if command.instruction is Instruction.RUN]
        if run_commands:
            if any(
                    'apt' in run_command.tokens
                    or 'apt-get' in run_command.tokens
                    for run_command in run_commands) \
                and all(
                    not token.startswith('BUILD_PACKAGES=')
                    for run_command in run_commands
                    for token in run_command.tokens):
                yield run_commands[0], []


apt_purge_variants = [
    [apt, 'purge', auto_remove, '-y', '$', '{', 'BUILD_PACKAGES', '}']
    for apt in ['apt-get', 'apt']
    for auto_remove in ['--autoremove', '--auto-remove']
]


@strict_policy
def YIP0016(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    If you need to install packages, use
    »apt-get purge --autoremove -y ${BUILD_PACKAGES}« to uninstall them
    properly afterwards,
    """
    for stage in dockerfile.stages:
        run_commands = [command for command in stage
                        if command.instruction is Instruction.RUN]
        if run_commands:
            all_tokens = \
                [run_command.tokens for run_command in run_commands]
            if any('apt' in tokens or 'apt-get' in tokens
                   for tokens in all_tokens) \
                and not any(
                is_infix(apt_purge, tokens)
                for tokens in all_tokens
                    for apt_purge in apt_purge_variants):
                yield run_commands[-1], []


@strict_policy
def YIP0017(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    If you use OpenStack source, you SHOULD use the official
    opendev.org repositories.
    """
    for stage in dockerfile.stages:
        for command in stage:
            tokens = command.tokens
            if 'git' in tokens and 'clone' in tokens \
                and any('openstack' in token for token in tokens) \
                and not any(token.startswith('//opendev.org')
                            for token in tokens):
                yield command, []


@hinting_policy
def YIP0018(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    After the build, files which are not required during run-time,
    MUST be cleaned up. This reduces the image size and lowers the risk
    of security holes. This includes for example:

    * removal of files copied into the container by using rm in a RUN
    instruction;

    * removal of source directories and other files only needed during
    container image build by using rm in a RUN instruction;

    * removal of cache in user home in a RUN instruction, e.g.
    rm -rf "${HOME}/.cache";

    * removal of further cache files;

    * removal of apt indices by using rm in a RUN instruction, e.g.
    rm -rf /var/lib/apt/lists/*;
    (There is no need to call apt-get clean as there is an apt-hook
    (/etc/apt/apt.conf.d/docker-clean) which handles this automatically.)
    """
    yield dockerfile.stages[-1][-1], []


@hinting_policy
def YIP0013(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    If possible you SHOULD add a USER statement at the end of OpenStack
    related images, so that the container will per default start with
    the target user (as given in
    https://docs.yaook.cloud/implementation_details/containers.html).
    """
    last_stage = dockerfile.stages[-1]
    if all(command.instruction != Instruction.USER for command in last_stage):
        yield last_stage[-1], []


@strict_policy
def YIP0014(dockerfile: Dockerfile) -> Policy_Evaluation:
    """
    You MUST provide a reason in the Dockerfile comments when using the
    ENTRYPOINT instruction.
    """
    last_stage = dockerfile.stages[-1]
    some_command = last(command for command in last_stage
                        if command.instruction is Instruction.ENTRYPOINT)
    if some_command is not None and not some_command.comment:
        yield some_command, []


# Dockerfile and policy processing

@dataclass
class Violation:
    """
    Abstract representation of a policy finding.

    :param p:       Path to dockerfile.
    :param line:    Line in dockerfile where relevant command resides.
    :param message: Line-wise descriptive message of the finding.
    :param policy:  Name of associated policy.
    """
    p: Path
    line: int
    message: list[str]
    policy: str


def parse_dockerfile(p: Path) -> Iterator[dict[str, Any]]:
    """Parses dockerfile at `p` into a raw abstract representation."""
    # DockerfileParser only accepts files named "Dockerfile";
    # hence the following indirect reading
    raw_commands = DockerfileParser(fileobj=io.StringIO(p.read_text()))
    structure = typing.cast(
        Iterator[dict[str, Any]],
        raw_commands.structure)
    for raw_command in structure:
        line_start = raw_command['startline']
        line_end = raw_command['endline']
        raw_instruction = raw_command.pop('instruction')
        if raw_instruction not in Instruction.__members__:
            fail('Unknown dockerfile instruction, '
                 f'in {p} lines {line_start} to {line_end}')
        raw_command['instruction'] = Instruction[raw_instruction]
        yield raw_command


re_embedded_comments = re.compile(r'([^`]*(?:``|`[^#][^`]*`)?)(?:`#[^`]*`)?')


def strip_embedded_comments(s: str) -> str:
    """
    Strips comments embedded into backtick command substitutions from
    shell-syntax `s`.  Helps to work-around that shlex.shlex cannot cope
    with nested comments in command substitutions and would strip
    everything following »#«"""
    return re_embedded_comments.sub(r'\1', s)


def split_tokens(s: str) -> list[str]:
    """Splits shell-syntax `s` into an approximate tokenization."""
    return list(shlex.shlex(s, posix=True, punctuation_chars=True))


policy_disable_prefix = 'yaook-image-policy'
policy_disable_directive = 'disable'


def splitoff_policy_directives(directive: str, comment_lines: list[str]) \
        -> Tuple[set[str], list[str]]:
    """
    Separate `comment_lines` into a pair consisting of
    * the arguments of each directive named `directive`
    * the remaining lines
    """
    directives = set()
    comments = []
    re_directive = re.compile(
        re.escape(policy_disable_prefix) + r'\s+'
        + re.escape(directive) + '=' + r'(\w+(?:,\w+)*)\b')
    for line in comment_lines:
        match = re_directive.match(line)
        if match is None:
            comments.append(line)
        else:
            for s in match.group(1).split(','):
                directives.add(s)
    return directives, comments


def distill_commands_with_comments(raw_commands: list[dict[str, Any]]) \
        -> Iterator[Command]:
    """
    Transforms raw representation of a dockerfile as returned by
    `parse_dockerfile` into a sequence of `Command`s with
    * `Instruction.RUN` commands’ shell syntaxes tokenized;
    * comments aligned to following commands proper;
    * directives extracted from comments.
    """
    for partition in split_after(
            lambda raw: raw['instruction'] is not Instruction.COMMENT,
            raw_commands):
        raw_last_command = partition[-1]
        last_instruction = raw_last_command['instruction']
        # special case: last command in dockerfile is comment
        if last_instruction is Instruction.COMMENT:
            comment = [raw['value'] for raw in partition]
            yield Command(
                instruction=Instruction.COMMENT,
                value='\n'.join(comment),
                tokens=[],
                comment=comment,
                line_start=partition[0]['startline'],
                line_end=raw_last_command['endline'],
                disabled_policies=set())
        else:
            value = raw_last_command['value']
            tokens = split_tokens(strip_embedded_comments(value)) \
                if last_instruction is Instruction.RUN else []
            disabled_policies, comment = \
                splitoff_policy_directives(
                    policy_disable_directive,
                    [raw['value'] for raw in partition[:-1]])
            yield Command(
                instruction=last_instruction,
                value=raw_last_command['value'],
                tokens=tokens,
                comment=comment,
                line_start=raw_last_command['startline'],
                line_end=raw_last_command['endline'],
                disabled_policies=disabled_policies)


def split_stages(commands: list[Command]) -> list[list[Command]]:
    """Splits `commands` into `stages`."""
    return split_before(
        lambda command: command.instruction is Instruction.FROM,
        commands)


def read_dockerfile(p: Path) -> Dockerfile:
    """Reads dockerfile at `p` into abstract representation."""
    raw_commands = list(parse_dockerfile(p))
    if not raw_commands:
        fail(f'Empty dockerfile: {p}')
    commands = list(distill_commands_with_comments(raw_commands))
    stages = split_stages(commands)
    return Dockerfile(p=p, stages=stages)


def find_violations(dockerfile: Dockerfile, strict: bool) \
        -> Iterator[Violation]:
    """
    Enumerates policy violations found in `dockerfile`; if `strict`,
    apply known strict policies, otherwise apply known hinting policies.
    """
    for policy in policies.values():
        if policy.strict != strict:
            continue
        for command, msg in policy.apply(dockerfile):
            if policy.name in command.disabled_policies:
                continue
            yield Violation(p=dockerfile.p,
                            line=command.line_start + 1,
                            message=policy.doc if not msg else msg,
                            policy=policy.name)


def check_expectations(dockerfile: Dockerfile) -> Iterator[Violation]:
    """
    For each command in `dockerfile` which does not violate policies
    explicitly suppressed by directives, yields a formal violation.
    """

    for policy in policies.values():
        expected_lines = set(
            command.line_start
            for stage in dockerfile.stages
            for command in stage
            if policy.name in command.disabled_policies)
        violated_lines = set(
            command.line_start
            for command, _ in policy.apply(dockerfile))
        for line_number in expected_lines - violated_lines:
            yield Violation(p=dockerfile.p,
                            line=line_number,
                            message=policy.doc,
                            policy=policy.name)


def find_files(p: Path, select: Callable[[str], bool]) -> Iterator[Path]:
    """
    Enumerates all files in `p` satisfying `select`; symbolic links
    pointing to duplicates are silently skipped.
    """
    resolved_files = set()
    for q in sorted(p.iterdir(), key=lambda p: (p.is_symlink(), str(p))):
        if q.is_file() and select(q.name):
            actual_file = q.resolve()
            if actual_file not in resolved_files:
                resolved_files.add(actual_file)
                yield q


def check_name(include: re.Pattern[str], exclude: list[str], name: str) \
        -> bool:
    """
    Computes whether `name` is matched by `include` and then
    not a member of `exclude`.
    """
    return include.match(name) is not None and name not in exclude


# Main

option_no_hints = '--no-hints'


def process_command_line(description: str) -> argparse.Namespace:
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        '--pattern',
        default=r'^Dockerfile.*$',
        help='Regular expression to identify the Dockerfiles to check')
    parser.add_argument(
        '--skip', nargs='+', default=[],
        help='File name to be skipped in any case.')
    parser.add_argument(
        option_no_hints, dest='hints', action='store_false',
        help='Report violations of strict policies only, '
        'no (speculative) hints.')
    parser.add_argument(
        '--check-expectations', action='store_true',
        help='Process expectation directives.')
    parser.add_argument(
        'dir', nargs='?', type=Path, default=Path(os.curdir),
        help='Directory with dockerfiles to check.')
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    return args


def report_violated_policies(violations: list[Violation]) -> None:
    message('WARNING! Image policy violations encountered.')
    for violation in violations:
        location = f'{violation.p}: {violation.line}' \
            if violation.line is not None else f'{violation.p}'
        message(
            f'{location}: {violation.message[0]} '
            f'(Annotate with »# {policy_disable_prefix} '
            f'{policy_disable_directive}={violation.policy}« '
            f'to suppress.)')
        for msg in violation.message[1:]:
            message(f'  {msg}')


def report_hinting_policies(violations: list[Violation]) -> None:
    message(
        f'The following hints might assist to conform to the image '
        f'policy. Use »{option_no_hints}« to suppress.')
    for violation in violations:
        location = f'{violation.p}: {violation.line}' \
            if violation.line is not None else f'{violation.p}'
        message(f'{location}: {violation.message[0]}')
        for msg in violation.message[1:]:
            message(f'  {msg}')


def report_violated_expectations(violations: list[Violation]) -> None:
    message('WARNING! Unfulfilled expectations of image policy violations.')
    for violation in violations:
        location = f'{violation.p}: {violation.line}' \
            if violation.line is not None else f'{violation.p}'
        message(
            f'{location}: Policy {violation.policy} expected '
            f'to be violated but passes.')


def main() -> None:
    import __main__
    args = process_command_line(__main__.__doc__.lstrip().split('\n\n')[0])
    select = partial(check_name, re.compile(args.pattern), args.skip)
    dockerfiles = \
        [read_dockerfile(p) for p in find_files(args.dir, select)]
    if not dockerfiles:
        fail('No dockerfiles found. '
             'This tool should only be used with Yaook image repositories!')
    report_exitcode = False
    for dockerfile in dockerfiles:
        violations = list(find_violations(dockerfile, strict=True))
        if violations:
            report_exitcode = True
            report_violated_policies(violations)
        if args.hints:
            violations = list(find_violations(dockerfile, strict=False))
            if violations:
                report_hinting_policies(violations)
        if args.check_expectations:
            violations = list(check_expectations(dockerfile))
            if violations:
                report_exitcode = True
                report_violated_expectations(violations)
    if report_exitcode:
        raise SystemExit(1)


# Main

if __name__ == '__main__':
    main()
